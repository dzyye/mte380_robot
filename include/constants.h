#define ENABLE_PRINT 0

// 1: Testing at Ye's house
// 0: On the course
#define TESTING_AT_HOME 0

// Ultrasonic Constants
#define INTAKE_DISTANCE 7

#if TESTING_AT_HOME
#define BACKUP_DISTANCE_FAR_WALL 60
#define BACKUP_DISTANCE_TARGET_WALL 22
#else
#define BACKUP_DISTANCE_FAR_WALL 124
#define BACKUP_DISTANCE_TARGET_WALL 16.5 // Subtract 12 when arcing
#endif

#define FINISH_WALL_DISTANCE 40

// Angle Constants
#if TESTING_AT_HOME
#define APPROACH_TARGET_ANGLE -35
#define INTAKE_TARGET_ANGLE -5.0
#else
#define APPROACH_TARGET_ANGLE -31
#define INTAKE_TARGET_ANGLE -13.0 // Not sure why this needs to be so angled on course
#endif
#define SAFE_ZONE_ANGLE 85 // 5 degree less than 90 to prevent overshoot
#define END_ANGLE_THRESHOLD 18


#define BACKUP_STRAIGHT_ANGLE (-5.0)
// Speed Constants
// speed: 190, kp: 1.3
#define DRIVE_RAMPUP (-50)
#define DRIVE_RAMPDOWN (110)
#define DRIVE_FIND_LINE_RAMPDOWN (70)


#define DRIVE_FORWARD_SPEED 200
#define DRIVE_TARGET_APPROACH_SPEED 85
#define DRIVE_FIND_LINE_SPEED 75
#define DRIVE_TURN_TO_CENTER_LINE_SPEED 45
#define DRIVE_BACKUP_SPEED 90
#define DRIVE_RETURN_TO_START_SPEED 75

#define DRIVE_TURN_SPEED 85
#define DRIVE_ARC_TURN_SPEED 110
#define DRIVE_ARC_TURN_RATIO 1.5 // Always greater than 1

// PID Constants
#define PID_APPROACH_FACTOR_KP (1.42)

#if TESTING_AT_HOME
#define PID_LINE_FACTOR_KP (0.29)
#else
#define PID_LINE_FACTOR_KP (1.46)
#endif
#define PID_LINE_FACTOR_KI (0.00)
#define PID_LINE_FACTOR_KD (0.02)
#define SPEED_DIFFERENCE_CONSTRAINT 56

#define PID_STRAIGHT_FACTOR_KP (3.0)
#define PID_STRAIGHT_FACTOR_KI (0.0)
#define PID_STRAIGHT_FACTOR_KD (0.0)

// Color offsets chosen to better center the line between sensors.
// (Robot slightly biased to the right side of line without offset)
#if TESTING_AT_HOME
#define COLOR_RIGHT_OFFSET_R 0
#define COLOR_RIGHT_OFFSET_G 0
#define COLOR_RIGHT_OFFSET_B 0
#else
#define COLOR_RIGHT_OFFSET_R 10
#define COLOR_RIGHT_OFFSET_G 20
#define COLOR_RIGHT_OFFSET_B 20
#endif

#define COLOR_LEFT_OFFSET_R 0
#define COLOR_LEFT_OFFSET_G 0
#define COLOR_LEFT_OFFSET_B 0

// Constrain hue
#if TESTING_AT_HOME
#define HUE_CONSTRAIN_VALUE (100.0)
#else
#define HUE_CONSTRAIN_VALUE (50.0)
#endif

// Logic debounce
#define APPROACH_DEBOUNCE_MS 50
#define LOST_RED_DEBOUNCE_MS 200