#pragma once

#define ANGLE_THRESHOLD_DEGREES 1.5
#define IMU_NORTH 0.0
#define IMU_EAST 90.0
#define IMU_SOUTH 180.0
#define IMU_WEST -90.0
/**
 * Function: setupIMU
 * 
 * Initializes IMU on i2c and enables Digital Motion Processor (DMP), which
 * allows yaw, pitch and roll to be accuratly calculated from accelerometer and 
 * gyroscope values. Also calibrates accelerometer and gyroscope to be zeroed 
 * in the beginning.
*/
void setupIMU();

/**
 * Function: getTurnAngle
 * 
 * @return: Turn angle in degrees relative to starting angle.
 * (CW: 0 to 180 before wrapping to -180, CCW: 0 to -180 before wrapping to 180)
*/
float getTurnAngle();