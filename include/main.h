#pragma once

#define DEBOUNCE_TIME_MS 50 // Time in millis for push button debouncing.

#define STATUS_LED_PIN PC3

typedef enum {
    ROBOT_CALIBRATE_BLACK,
    ROBOT_CALIBRATE_WHITE,
    ROBOT_READY,
    ROBOT_FOLLOW_LINE,
    ROBOT_APPROACH_TARGET,
    ROBOT_INTAKE,
    ROBOT_BACKUP,
    ROBOT_OUTTAKE,
    ROBOT_FIND_LINE,
    ROBOT_FINISHED
} RobotState;

/**
 * Function: doCalibrateBlack
 * 
 * ROBOT_CALIBRATE_BLACK state function. Stores RGB min values when button pressed.
 * Transition: Go to ROBOT_CALIBRATE_WHITE when button pressed.
*/
void doCalibrateBlackState();

/**
 * Function: doCalibrateWhite
 * 
 * ROBOT_CALIBRATE_WHITE state function. Stores RGB max values when button pressed.
 * Transition: Go to ROBOT_READY when button pressed.
*/
void doCalibrateWhiteState();

/**
 * Function: doReadyState
 * 
 * ROBOT_READY state function. Waits until button pressed.
 * Transition: Go to ROBOT_FOLLOW_LINE when button pressed.
*/
void doReadyState();

/**
 * Function: doFollowLineState
 * 
 * ROBOT_FOLLOW_LINE state function. Follows line until close to target,
 * or the start zone is reached.
 * Transitions:
 *  - Go to ROBOT_APPROACH_TARGET when IMU detects close to target.
 *  - Go to ROBOT_FINISHED when both sensors see red and bullseye has already been reached.
*/
void doFollowLineState();

/**
 * Function: doApproachTargetState
 * 
 * ROBOT_APPROACH_TARGET state function. Follows line slowly until the target. 
 * 
 * Transitions:
 *  - Go to ROBOT_INTAKE when either sensor sees blue.
*/
void doApproachTargetState();

/**
 * Function: doIntakeState
 * 
 * ROBOT_INTAKE state function. Starts intake motors and robot crawls forward.
 * Transition: Go to ROBOT_BACKUP when limit switch is pressed.
*/
void doIntakeState();

/**
 * Function: doBackupState
 * 
 * ROBOT_BACKUP state function. Robot backs up from target to safe zone.
 * Transition: Go to ROBOT_OUTTAKE when backup procedure is finished.
*/
void doBackupState();

/**
 * Function: doOuttakeState
 * 
 * ROBOT_OUTTAKE state function. Robot spins conveyor until lego figure is dropped off.
 * Transition: Go to ROBOT_FIND_LINE when limit switch released for 3 seconds.
*/
void doOuttakeState();

/**
 * Function: doFindLineState
 * 
 * ROBOT_FIND_LINE state function. Robot crawls forward until it finds line.
 * Transition: Go to ROBOT_FOLLOW_LINE when right sensor sees red.
*/
void doFindLineState();

/**
 * Function: doFinishedState
 * 
 * ROBOT_FINISHED state function. Stop motors.
 * No transition (terminal state).
*/
void doFinishedState();
