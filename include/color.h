#pragma once

#define LEFT_RED_HUE_THRESHOLD 8
#define RIGHT_RED_HUE_THRESHOLD 8

#define COLOR_READ_TIME_MS 3
#define COLOR_MAX_VALUE 255

#define EPSILON (0.0001)
#define RED_THRESHOLD (20)
#define BLUE_THRESHOLD (30)
#define BLUE (-120.0) // -180 < Hue < 180 for this project
#define FULL_COLOR_WHEEL (360.0)
#define HALF_COLOR_WHEEL (180.0)

#define COLOR_CALIBRATION_LEFT_MIN {60, 69, 53}
#define COLOR_CALIBRATION_LEFT_MAX {150, 180, 125}

#define COLOR_CALIBRATION_RIGHT_MIN {60, 65, 48}
#define COLOR_CALIBRATION_RIGHT_MAX {150, 170, 120}

typedef struct
{
    float r;
    float g;
    float b;
} Rgb;

typedef struct
{
    Rgb min = Rgb{0, 0, 0};
    Rgb max = Rgb{COLOR_MAX_VALUE, COLOR_MAX_VALUE, COLOR_MAX_VALUE};
} ColorSensorCal;

void setupColorSensor();

void printRGB(const Rgb &value);

/**
 * Function: readColorValues
 * 
 * Read the current rgb values from both color sensors into the structs indicated by the arguments.
 * 
 * @arg left_value: Struct to write the left color values to.
 * @arg right_value: Struct to write the right color values to.
 * @arg read_raw_values: If true, do not map/constrain the rgb values.
 *      This will need to be set to true when reading calibration white/black.
 * 
 * @returns: true if values were read successfully. false otherwise.
*/
bool readColorValues(Rgb &left_value, Rgb &right_value, bool read_raw_values = false);

/**
 * Function: computeHue
 * 
 * Converts rgb triplet to hue value. Red is 0 (360), green is 120, blue is 240.
 * Algorithm can be found from this stackerflow question:
 * https://stackoverflow.com/questions/23090019/fastest-formula-to-get-hue-from-rgb.
 * 
 * @arg &rgb: Reference to RGB struct to be converted to hue.
 * 
 * @returns: Computed hue value.
*/
float computeHue(const Rgb &rgb);

/**
 * Function: getHues
 * 
 * Reads from the two color sensors, returning the hue by reference.
 * 
 * @arg &left_hue: Reference to left hue.
 * @arg &right_hue: Reference to right hue.
 * 
 * @return: Whether or not the color read was successful.
*/
bool getHues(float &left_hue, float &right_hue);

/**
 * Function: calibrateBlack
 * 
 * Calibrates the minimum values of the color sensor by reading from all black.
*/
void calibrateBlack();

/**
 * Function: calibrateWhite
 * 
 * Calibrates the maximum values of the color sensor by reading from all white.
*/
void calibrateWhite();
