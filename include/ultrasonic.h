#pragma once

/**
 * Trigger pin initiates ultrasonic pulse when set high.
 * Echo pin goes high when pulse is sent and back to low when echo is received.
 * Since trigger and echo signals follow each other sequentially, both functions
 * can be assigned to the same pin.
*/
#define ULTRASONIC_TRIGGER_PIN  PA10
#define ULTRASONIC_ECHO_PIN     PA10
#define ULTRASONIC_MAX_DISTANCE 400 // Max distance in cm

#define DISTANCE_THRESHOLD_CM 0.25
/**
 * Low pass filter used to smoothen ultrasonic data.
 * LPF_FACTOR = 0 means no filter (raw sensor readings), fastest response.
 * Higher LPF_FACTOR results in previous readings influencing current readings
 * more, causing slower response.
*/
#define LPF_FACTOR 0.2

/**
 * Function: getDistance
 * 
 * @return: Distance in cm, and 0.0 if invalid reading.
*/
float getDistance();
