/**
 * Shared functionality between drive and intake motor modules.
*/
#pragma once

#define ANALOG_HIGH 255
#define ANALOG_LOW 0

typedef enum {
    MOTOR_BREAK,
    MOTOR_COAST
} MotorMode;

/**
 * Function: _motorRun
 * 
 * Moves a motor specified by pins pin1 and pin2 at a constant speed.
 * 
 * @arg speed: Speed of the motor
 * @arg mode: Either break or coast mode
 * @arg pin1: First analog pin for relevant motor (mode or speed)
 * @arg pin2: Second analog pin for relevant motor (mode or speed)
*/
void motorRun(int speed, MotorMode mode, int pin1, int pin2);
