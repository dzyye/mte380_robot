#pragma once

#define INTAKE_SPEED 255
#define IN_CONVEYOR_SPEED 200
#define OUT_CONVEYOR_SPEED 200

#define LIMIT_SWITCH_DEBOUNCE 50

#define CONVEYOR_PIN_2 PA6
#define CONVEYOR_PIN_1 PA5
#define INTAKE_PIN_2 PA7
#define INTAKE_PIN_1 PA9
            
/**
 * Function: setupIntake
 * 
 * Set the pins for the intake motors. 
*/
void setupIntake();

/**
 * Function: intakeRun
 * 
 * Run the intake spinner and conveyor motors at the speeds specified above.
*/
void intakeRun(int intake_speed = INTAKE_SPEED, int conveyor_speed = IN_CONVEYOR_SPEED);

/**
 * Function: intakeStop
 * 
 * Stop the intake spinner and conveyor belt.
*/
void intakeStop();
