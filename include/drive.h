#pragma once

#include "motorHelper.h"

// From top to bottom of motor driver (bottom is big capacitor)
#define MOTOR_LEFT_PIN_2 D10 // Not using D5 because it gets pulled high briefly on startup
#define MOTOR_LEFT_PIN_1 D9
#define MOTOR_RIGHT_PIN_2 D6
#define MOTOR_RIGHT_PIN_1 D3

typedef enum {
    CLOCKWISE,
    COUNTERCLOCKWISE
} TurnDirection;

/**
 * Function: setupMotor
 * 
 * Sets up the pins for the motor.
*/
void setupMotor();

/**
 * Function: setLeftMotorSpeed
 * 
 * Runs the left motor at a constant speed.
 * 
 * @arg speed: Speed of the motor from -255 to 255. Positive speed is forward, negative is backwards.
 * @arg mode: Either break or coast mode
*/
void setLeftMotorSpeed(int speed, MotorMode mode = MOTOR_BREAK);

/**
 * Function: setRightMotorSpeed
 * 
 * Runs the right motor at a constant speed.
 * 
 * @arg speed: Speed of the motor from -255 to 255. Positive speed is forward, negative is backwards.
 * @arg mode: Either break or coast mode
*/
void setRightMotorSpeed(int speed, MotorMode mode = MOTOR_BREAK);

/**
 * Function: driveStop
 * 
 * Stops both motors.
*/
void driveStop();

/**
 * Function: driveToDistance
 * 
 * Drives forwards/backwards until the the desired distance from ultrasonic sensor.
 * Automatically decides to go forwards or backwards based on current distance.
 * 
 * @arg speed: Forward or backwards speed
 * @arg target_distance: Desired distance in cm
*/
void driveToDistance(float speed, float target_distance);

/**
 * Function: turnToAngle
 * 
 * Turns clockwise/counter clockwise until the desired angle is reached.
 * Automatically decides to turn clockwise or counterclockwise depending on which direction is shorter.
 * 
 * @arg turn_speed: Turning speed (set to negative for backwards arcing turn)
 * @arg target_angle: Desired angle in degrees
 * @arg arc_turn: set to true to enable turning in an arc (both motors running in same direction)
*/
void turnToAngle(float turn_speed, float target_angle, bool arc_turn = false);

/**
 * Function: followLinePid
 * 
 * Follow the red line using PID. Currently uses the difference between the two colour sensors to judge colour.
 * 
 * @arg error: Difference between right and left hue readings
 * @arg motor_speed: Base speed to use for the motor.
*/
void drivePid(float error, float kp, float kd, float ki, float motor_speed);