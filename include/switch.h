#pragma once

#define DEBOUNCE_TIME_MS 50

#define LIMIT_SWITCH PB5
#define PUSH_BUTTON USER_BTN

#define PRESSED false
#define RELEASED true

typedef struct {
    bool prev_state = RELEASED;
    bool current_state = RELEASED;
    int prev_time = 0;
} SwitchStatus;

/**
 * Function: setupSwitches
 *
 * Sets the limit switch as output.
*/
void setupSwitches();

/**
 * Function: checkButtonPressed
 * 
 * Check if the user button is pressed. Accounts for debouncing. Should be run once (or more) per loop to act as polling.
 * 
 * @return true if pressed, false if unpressed
*/
bool checkButtonPressed();

/**
 * Function: checkSwitchPressed
 * 
 * Check if the limit switch is pressed. Accounts for debouncing. Should  be run once (or more) per loop to act as polling.
 *
 * @return true if pressed, false if unpressed
*/
bool checkSwitchPressed();
