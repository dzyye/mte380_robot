import matplotlib.pyplot as plt
import numpy as np
from re import split

import matplotlib as mpl

sign = lambda x: -1 if x < 0 else 1

filename = "11-12-readings.txt"

# Store data
data = {
    "left": {
        "r": [],
        "g": [],
        "b": [],
        "hue": []
    },
    "right": {
        "r": [],
        "g": [],
        "b": [],
        "hue": []
    }
}

# Parse the log file into the data global variable
def parseFile():
    lines = []
    with open(filename) as f:
        lines = [split(r"\D+", line.strip()) for line in f]
        
        
    for line in lines:
        line = [int(num) for num in line[1:]]
        data["left"]["r"].append(line[0])
        data["left"]["g"].append(line[1])
        data["left"]["b"].append(line[2])
        # Wrap hue to -120 to 240
        hue_l = line[3]
        data["left"]["hue"].append(hue_l if hue_l < 240 else hue_l - 360)
        data["right"]["r"].append(line[4])
        data["right"]["g"].append(line[5])
        data["right"]["b"].append(line[6])
        # Wrap hue to -120 to 240
        hue_r = line[7]
        data["right"]["hue"].append(hue_r if hue_r < 240 else hue_r - 360)

def plotLeftRight(ax, data_key, color):
    """
    Plot the left/right data based on data_key
        ax: matplotlib axis object
        data_key: "r", "g", "b", or "hue" 
        color: "r", "g", "b", "k"
    """
    time = range(len(data["left"][data_key]))
    plt_left = ax.plot(time, data["left"][data_key], color + "-")
    plt_right = ax.plot(time, data["right"][data_key], color + "--")
    return (plt_left, plt_right) 

def transform_error(e, max_error):
    """
    Function to transform error, for linearization
    """
    
    # Not sure of needed - if error is small, do not bother transforming
    if abs(e) < 4:
        return e
    
    # Exponent for error term 
    exp = 0.50
    # Coefficient for error term (about 7.28)
    coeff = max_error / ((max_error) ** exp)
    # print(coeff)
    return coeff * (abs(e) ** (exp))*sign(e);
    
def main():
    parseFile()
    fig, axes = plt.subplots(2, 1)
    time = range(len(data["left"]["hue"]))
    # plotLeftRight(axes[0], "r", "r")
    # plotLeftRight(axes[1], "g", "g")
    # plotLeftRight(axes[2], "b", "b")
    plotLeftRight(axes[0], "hue", "k")
    
    error = [r - l for l, r in zip(data["right"]["hue"], data["left"]["hue"])]
    max_error = max(abs(e) for e in error)
    error_transformed = [transform_error(e, max_error) for e in error]

    # axes[1].plot(time, error_transformed, "k-")
    axes[1].plot(time, error, "k-")
    # axes[1].set_title("Error vs Lateral position")
    axes[1].set_ylabel("Measured Error")
    axes[1].set_xlabel("Robot Lateral Position")
    print(filename.split('.')[0])
    plt.savefig(filename.split('.')[0]+".png", dpi=800)
        
if __name__ == "__main__":
    main()