#include "motorHelper.h"
#include "intake.h"
#include <Arduino.h>

void setupIntake() {
    pinMode(INTAKE_PIN_1, OUTPUT);
    pinMode(INTAKE_PIN_2, OUTPUT);
    pinMode(CONVEYOR_PIN_1, OUTPUT);
    pinMode(CONVEYOR_PIN_2, OUTPUT);
}

void intakeRun(int intake_speed, int conveyor_speed) {
    motorRun(intake_speed, MOTOR_BREAK, INTAKE_PIN_1, INTAKE_PIN_2);
    motorRun(conveyor_speed, MOTOR_BREAK, CONVEYOR_PIN_1, CONVEYOR_PIN_2);
}

void intakeStop() {
    motorRun(0, MOTOR_BREAK, INTAKE_PIN_1, INTAKE_PIN_2);
    motorRun(0, MOTOR_BREAK, CONVEYOR_PIN_2, CONVEYOR_PIN_2);
}
