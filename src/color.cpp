#include <Arduino.h>
#include "color.h"
#include "Adafruit_TCS34725.h"
#include "constants.h"

// Initiate color sensor instances
static Adafruit_TCS34725 color_sensor_left = Adafruit_TCS34725(TCS34725_INTEGRATIONTIME_2_4MS, TCS34725_GAIN_60X);
static Adafruit_TCS34725 color_sensor_right = Adafruit_TCS34725(TCS34725_INTEGRATIONTIME_2_4MS, TCS34725_GAIN_60X);

static TwoWire Wire2 = TwoWire(PC9, PA8); // I2C channel 3 for right sensor

static ColorSensorCal color_cal_left{COLOR_CALIBRATION_LEFT_MIN, COLOR_CALIBRATION_LEFT_MAX};
static ColorSensorCal color_cal_right{COLOR_CALIBRATION_RIGHT_MIN, COLOR_CALIBRATION_RIGHT_MAX};

static bool reading_started = false;
static int start_reading_millis = 0;

void printRGB(const Rgb &value)
{
    Serial.print("R:");
    Serial.print(int(value.r));
    Serial.print(" G:");
    Serial.print(int(value.g));
    Serial.print(" B:");
    Serial.print(int(value.b));
}

bool readColorValues(Rgb &left_value, Rgb &right_value, bool read_raw_values)
{
    // Turn on LEDs
    color_sensor_left.setInterrupt(false);
    color_sensor_right.setInterrupt(false);

    if (reading_started == false) {
        reading_started = true;
        start_reading_millis = millis();
    } else {
        // Read only after 24ms
        if (millis() - start_reading_millis > COLOR_READ_TIME_MS) {
            color_sensor_left.getRGB(&left_value.r, &left_value.g, &left_value.b);
            color_sensor_right.getRGB(&right_value.r, &right_value.g, &right_value.b);
            // Turn off LEDs
            color_sensor_left.setInterrupt(true);
            color_sensor_right.setInterrupt(true);

            if (!read_raw_values)
            {
                // Calibrate left color value
                left_value.r = constrain(map(left_value.r, color_cal_left.min.r, color_cal_left.max.r, 0, COLOR_MAX_VALUE) + COLOR_LEFT_OFFSET_R, 0, COLOR_MAX_VALUE);
                left_value.g = constrain(map(left_value.g, color_cal_left.min.g, color_cal_left.max.g, 0, COLOR_MAX_VALUE) + COLOR_LEFT_OFFSET_G, 0, COLOR_MAX_VALUE);
                left_value.b = constrain(map(left_value.b, color_cal_left.min.b, color_cal_left.max.b, 0, COLOR_MAX_VALUE) + COLOR_LEFT_OFFSET_B, 0, COLOR_MAX_VALUE);

                // Calibrate right color value
                right_value.r = constrain(map(right_value.r, color_cal_right.min.r, color_cal_right.max.r, 0, COLOR_MAX_VALUE) + COLOR_RIGHT_OFFSET_R, 0, COLOR_MAX_VALUE);
                right_value.g = constrain(map(right_value.g, color_cal_right.min.g, color_cal_right.max.g, 0, COLOR_MAX_VALUE) + COLOR_RIGHT_OFFSET_G, 0, COLOR_MAX_VALUE);
                right_value.b = constrain(map(right_value.b, color_cal_right.min.b, color_cal_right.max.b, 0, COLOR_MAX_VALUE) + COLOR_RIGHT_OFFSET_B, 0, COLOR_MAX_VALUE);
            }

            reading_started = false;
        }
    }
    return !reading_started;
}

float computeHue(const Rgb &rgb)
{
    float red = rgb.r / 255;
    float green = rgb.g / 255;
    float blue = rgb.b / 255;

    float max_component = max(max(red, green), blue);
    float min_component = min(min(red, green), blue);

    float hue;

    float diff = max_component - min_component;

    if (abs(diff) < EPSILON) return 0; 

    if (max_component == red)
    {
        hue = (green - blue) / (diff) + (green < blue ? 6.0 : 0);
    }
    else if (max_component == green)
    {
        hue = (blue - red) / (diff) + 2.0;
    }
    else
    {
        hue = (red - green) / (diff) + 4.0;
    }

    return hue * 60;
}

bool getHues(float &left_hue, float &right_hue) {
    Rgb left_color = {};
    Rgb right_color = {};
    // Read RGB
    if(readColorValues(left_color, right_color)) {
        float left = computeHue(left_color);
        float right = computeHue(right_color);

        // Constrain hue to -180 to +180
        left_hue = left > HALF_COLOR_WHEEL ? left - FULL_COLOR_WHEEL : left;
        right_hue = right > HALF_COLOR_WHEEL ? right - FULL_COLOR_WHEEL : right;
#if ENABLE_PRINT
        Serial.print(" Left ");
        printRGB(left_color);
        Serial.print(" Hue: ");
        Serial.print(int(left_hue));
        Serial.print(" ");
        Serial.print("Right ");
        printRGB(right_color);
        Serial.print(" Hue: ");
        Serial.println(int(right_hue));
#endif
        return true;
    } else {
        // RGB read was unsuccessful
        return false;
    }
}

void setupColorSensor()
{
    // Color sensor setup
    // Left sensor on pins SDA=D14 SCK=D15 (default I2C channel 1)
    if (color_sensor_left.begin())
    {
        Serial.println("Found left color sensor");
    }
    else
    {
        Serial.println("Left color sensor not found");
        while (1)
            ;
    }

    // Right sensor on pins SDA=PC9 SCK=PA8
    if (color_sensor_right.begin(TCS34725_ADDRESS, &Wire2))
    {
        Serial.println("Found right color sensor");
    }
    else
    {
        Serial.println("Right color sensor not found");
        while (1)
            ;
    }
};

void calibrateBlack()
{
    // Calibrate black
    while(!readColorValues(color_cal_left.min, color_cal_right.min, true));
}

void calibrateWhite()
{
    // Calibrate white
    while(!readColorValues(color_cal_left.max, color_cal_right.max, true));
}