#include <Arduino.h>
#include "ultrasonic.h"
#include <NewPing.h>

// Used to filter out invalid distance readings (sensor returns 0.0 for invalid reading)
#define DISTANCE_EPSILON 0.0001

// Ultrasonic sensor
static NewPing ultrasonic_sensor(ULTRASONIC_TRIGGER_PIN, ULTRASONIC_ECHO_PIN, ULTRASONIC_MAX_DISTANCE);
static float prev_distance_value = 0.0;

float getDistance() {
    // Send ping, get distance in cm
    float curr_distance = (float)(ultrasonic_sensor.ping()) / US_ROUNDTRIP_CM;
    if (curr_distance > DISTANCE_EPSILON) {
        // Only filter a valid reading
        curr_distance = LPF_FACTOR * prev_distance_value + (1-LPF_FACTOR) * curr_distance; 
    } else {
        // Invalid reading, use previous distance
        curr_distance = prev_distance_value;
    }
    prev_distance_value = curr_distance;
    return curr_distance;
}