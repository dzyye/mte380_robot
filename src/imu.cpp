#include <Arduino.h>
#include "imu.h"

#include "MPU6050_6Axis_MotionApps20.h"

static MPU6050 imu;
static Quaternion q;           // [w, x, y, z]         quaternion container
static VectorFloat gravity;    // [x, y, z]            gravity vector
static uint8_t fifoBuffer[64]; // FIFO storage buffer
static float x, y, z;

static float ypr[3];           // [yaw, pitch, roll]   yaw/pitch/roll container and gravity vector

void setupIMU() {
    // Initialize IMU on i2c
    imu.initialize();
    Serial.println(imu.testConnection() ? "MPU6050 connection successful" : "MPU6050 connection failed");
    // Initialize DMP to perform yaw, pitch, roll calculations
    imu.dmpInitialize();
    // Calibrate accelerometer and gyroscope
    imu.CalibrateAccel(6);
    imu.CalibrateGyro(6);
    imu.setDMPEnabled(true);
}

float getTurnAngle() {
    // If getting the latest packet was successful, return the new yaw value
    // Otherwise, return the previous yaw value
    if (imu.dmpGetCurrentFIFOPacket(fifoBuffer)) {
        imu.dmpGetQuaternion(&q, fifoBuffer);
        imu.dmpGetGravity(&gravity, &q);
        imu.dmpGetYawPitchRoll(ypr, &q, &gravity);
    }
    // Only return yaw
    return ypr[0] * 180/M_PI;
}