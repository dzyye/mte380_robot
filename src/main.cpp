#include <Arduino.h>

#include "main.h"
#include "color.h"
#include "drive.h"
#include "ultrasonic.h"
#include "imu.h"
#include "switch.h"
#include "intake.h"
#include "constants.h"

// Flag to prevent ROBOT_FOLLOW_LINE from going to ROBOT_FINISHED before reaching target
bool reached_target = false;

// Flag set to true when right color sensor finds line
bool found_line = false;

float left_hue = 0;
float right_hue = 0;

bool started_approach = false;

// Start at ROBOT_READY if color sensors already calibrated
RobotState robot_state = ROBOT_READY;

void setup()
{
    Serial.begin(115200);

    // Setup status LED pin
    pinMode(STATUS_LED_PIN, OUTPUT);
    digitalWrite(STATUS_LED_PIN, LOW);

    setupMotor();

    setupColorSensor();

    setupIMU();

    setupSwitches();

    setupIntake();

    // Turn on LED to signal end of setup and IMU calibration
    digitalWrite(STATUS_LED_PIN, HIGH);
}

void loop()
{
    switch (robot_state)
    {
        case ROBOT_CALIBRATE_BLACK:
        {
            doCalibrateBlackState();
            break;
        }
        case ROBOT_CALIBRATE_WHITE:
        {
            doCalibrateWhiteState();
            break;
        }
        case ROBOT_READY:
        {
            doReadyState();
            break;
        }
        case ROBOT_FOLLOW_LINE:
        {
            doFollowLineState();
            break;
        }
        case ROBOT_APPROACH_TARGET:
        {
            doApproachTargetState();
            break;
        }
        case ROBOT_INTAKE:
        {
            doIntakeState();
            break;
        }
        case ROBOT_BACKUP:
        {
            doBackupState();
            break;
        }
        case ROBOT_OUTTAKE:
        {
            doOuttakeState();
            break;
        }
        case ROBOT_FIND_LINE:
        {
            doFindLineState();
            break;
        }
        case ROBOT_FINISHED:
        default:
        {
            doFinishedState();
            break;
        }
    }
}

void doCalibrateBlackState()
{
    if (checkButtonPressed())
    {
#if ENABLE_PRINT
        Serial.print("Calibrating black");
#endif
        calibrateBlack();
        robot_state = ROBOT_CALIBRATE_WHITE;
    }
}

void doCalibrateWhiteState()
{
    if (checkButtonPressed())
    {
#if ENABLE_PRINT
        Serial.print("Calibrating white");
#endif
        calibrateWhite();
        robot_state = ROBOT_READY;
    }
}

void doReadyState()
{
    if (checkButtonPressed())
    {
#if ENABLE_PRINT
        Serial.println("Start line following to target");
#endif
        robot_state = ROBOT_FOLLOW_LINE;
    }
}

void doFollowLineState()
{
    static int approach_time = 0;
    static int rampup = DRIVE_RAMPUP; // start below full forward speed
    float motor_speed;
    if (reached_target)
    {
        // Driving back - check if reached start position.
        if (getDistance() < FINISH_WALL_DISTANCE && (abs(getTurnAngle() - IMU_WEST) < END_ANGLE_THRESHOLD))
        {
            robot_state = ROBOT_FINISHED;
#if ENABLE_PRINT
            Serial.println("Finished!");
#endif
            return;
        }
        motor_speed =  DRIVE_RETURN_TO_START_SPEED;
    }
    else
    {
        // Driving forward - check if close to target.
        if (getTurnAngle() < APPROACH_TARGET_ANGLE)
        {
            // Check for debouncing
            if (approach_time == 0) {
                approach_time = millis();
            } else if (millis() - approach_time > APPROACH_DEBOUNCE_MS) {
                robot_state = ROBOT_APPROACH_TARGET;
            }
#if ENABLE_PRINT
            Serial.println("Approaching Target");
#endif
        } else {
            approach_time = 0;
        }

        motor_speed = DRIVE_FORWARD_SPEED + rampup;

        if(rampup < 0) {
            rampup++;
        }
    }
    // Serial.print("1: ");
    // Serial.println(millis() - time);
    // Read hues to the global hue variables. If unsuccessful, early return.
    if (!getHues(left_hue, right_hue))
        return;

    // Serial.print("2: ");
    // Serial.println(millis() - time);
    // Execute main PID control loop.
    // right_hue = constrain(right_hue, -HUE_CONSTRAIN_VALUE, HUE_CONSTRAIN_VALUE);
    // left_hue = constrain(left_hue, -HUE_CONSTRAIN_VALUE, HUE_CONSTRAIN_VALUE);
    float error = (right_hue - left_hue);

    // float motor_speed = reached_target ? DRIVE_RETURN_TO_START_SPEED : DRIVE_FORWARD_SPEED;

    // motor_speed *= (1.1 - abs(error) * PID_LINE_FACTOR_KP / 120);
    drivePid(error, PID_LINE_FACTOR_KP, PID_LINE_FACTOR_KD, PID_LINE_FACTOR_KI, motor_speed);
    // Serial.print("3: ");
    // Serial.println(millis() - time);
}

void doApproachTargetState()
{
    static int base_speed = DRIVE_RAMPDOWN;
    // Read hues to the global hue variables. If unsuccessful, early return.
    if (!getHues(left_hue, right_hue))
        return;

    if (abs(left_hue - BLUE) < BLUE_THRESHOLD || abs(right_hue - BLUE) < BLUE_THRESHOLD)
    {
        // One color sensor detected blue, robot at target, proceed to intake state
        reached_target = true;
        driveStop();
#if ENABLE_PRINT
        Serial.println("Start intake");
#endif
        robot_state = ROBOT_INTAKE;

        return;
    }

    float error = right_hue - left_hue;

    if(base_speed > 0) {
        base_speed-=4;
    }

    // Scale motor speed based on error (larger error = drive slower)
    // This is required to be safer on the last turn.
    float motor_speed = (DRIVE_TARGET_APPROACH_SPEED + base_speed) * (1 - abs(error) / 100);
    // float motor_speed = DRIVE_TARGET_APPROACH_SPEED;
    drivePid(error, PID_APPROACH_FACTOR_KP, PID_LINE_FACTOR_KD, PID_LINE_FACTOR_KI, motor_speed);
}

void doIntakeState()
{
    // Start running intake/conveyor motors
    intakeRun();
    // Straighten robot perpendicular to pickup target
    turnToAngle(DRIVE_TURN_SPEED, INTAKE_TARGET_ANGLE);
    // Crawl forward until set distance away from wall, or limit switch has been pressed
    setLeftMotorSpeed(DRIVE_TARGET_APPROACH_SPEED);
    setRightMotorSpeed(DRIVE_TARGET_APPROACH_SPEED);
    // while limit switch not pressed, continue to crawl until <5cm from wall

    while (!checkSwitchPressed())
    {
        float current_distance = getDistance();
        // Serial.println(current_distance);
        if (current_distance < INTAKE_DISTANCE)
        {
            driveStop();
        } else {
            setLeftMotorSpeed(DRIVE_TARGET_APPROACH_SPEED);
            setRightMotorSpeed(DRIVE_TARGET_APPROACH_SPEED);
        }
    }
    driveStop();
    intakeStop();
#if ENABLE_PRINT
    Serial.println("Start backup");
#endif
    robot_state = ROBOT_BACKUP;
}

void doBackupState()
{
    // Fully straighten perpendicular to target wall before backing up
    turnToAngle(DRIVE_TURN_SPEED, BACKUP_STRAIGHT_ANGLE);
    // Backup a bit from target wall
    driveToDistance(DRIVE_BACKUP_SPEED, BACKUP_DISTANCE_TARGET_WALL);
    // Turn to align with safe zone
    turnToAngle(-DRIVE_ARC_TURN_SPEED, SAFE_ZONE_ANGLE, true);
    // Backup from far wall to reach safe zone
    driveToDistance(DRIVE_BACKUP_SPEED, BACKUP_DISTANCE_FAR_WALL);
#if ENABLE_PRINT
    Serial.println("Start outtake");
#endif
    robot_state = ROBOT_OUTTAKE;
}

void doOuttakeState()
{
    // Run conveyor to outtake the figure.
    intakeRun(0, OUT_CONVEYOR_SPEED);
    // After switch is released, delay for 500ms.
    while (!digitalRead(LIMIT_SWITCH));
    delay(500);
    intakeStop();
#if ENABLE_PRINT
    Serial.println("Start find line");
#endif
    robot_state = ROBOT_FIND_LINE;
}

void doFindLineState()
{
    static int base_speed = DRIVE_FIND_LINE_RAMPDOWN;
    static int lost_red_time = 0;

    if (!getHues(left_hue, right_hue))
        return;

    float right_red_error = right_hue < HALF_COLOR_WHEEL ? right_hue : FULL_COLOR_WHEEL - right_hue;

    if (found_line)
    {
        // If line has been found by the red sensor, wait until right sensor no longer detects red, then change state to start following line back to start.
        if (right_red_error > RED_THRESHOLD)
        {
            // Check to make sure red line has been lost after a delay
            if(lost_red_time == 0) {
                lost_red_time = millis();
            } else if(millis() - lost_red_time > LOST_RED_DEBOUNCE_MS) {
                driveStop();
#if ENABLE_PRINT
                Serial.println("Follow line back to target");
#endif
                robot_state = ROBOT_FOLLOW_LINE;
                return;
            }
        } else {
            lost_red_time = 0;
        }
    }
    else
    {
        if (base_speed > 0) {
            base_speed-=4;
        }
        // If line has not been found, crawl forward until right sensor detects red
        float error = getTurnAngle() - IMU_EAST;

        drivePid(error, PID_STRAIGHT_FACTOR_KP, PID_STRAIGHT_FACTOR_KD, PID_STRAIGHT_FACTOR_KI, DRIVE_FIND_LINE_SPEED + base_speed);

        // When right color sensor detects red, turn right until it no longer detects red
        // This ensures robot is straddling line before going to line following state
        if (right_red_error < RED_THRESHOLD && !found_line)
        {
            found_line = true;
            setLeftMotorSpeed(DRIVE_TURN_TO_CENTER_LINE_SPEED);
            setRightMotorSpeed(0);
        }
    }
}

void doFinishedState() {
    driveStop();
    // job done!
}