#include <Arduino.h>
#include "drive.h"
#include "imu.h"
#include "ultrasonic.h"
#include "motorHelper.h"
#include "constants.h"

void setupMotor()
{
    pinMode(MOTOR_LEFT_PIN_1, OUTPUT);
    pinMode(MOTOR_LEFT_PIN_2, OUTPUT);
    pinMode(MOTOR_RIGHT_PIN_1, OUTPUT);
    pinMode(MOTOR_RIGHT_PIN_2, OUTPUT);
}

void setLeftMotorSpeed(int speed, MotorMode mode)
{
    motorRun(speed, mode, MOTOR_LEFT_PIN_1, MOTOR_LEFT_PIN_2);
}

void setRightMotorSpeed(int speed, MotorMode mode)
{
    motorRun(speed, mode, MOTOR_RIGHT_PIN_1, MOTOR_RIGHT_PIN_2);
}

void driveStop() {
    motorRun(0, MOTOR_BREAK, MOTOR_LEFT_PIN_1, MOTOR_LEFT_PIN_2);
    motorRun(0, MOTOR_BREAK, MOTOR_RIGHT_PIN_1, MOTOR_RIGHT_PIN_2);
}

void driveToDistance(float speed, float target_distance) {
    float curr_distance = getDistance();
    bool forwards = true;
    if (curr_distance < target_distance) {
        // Move backwards
        setLeftMotorSpeed(-speed);
        setRightMotorSpeed(-speed);
        forwards = false;
    } else {
        // Move forwards
        setLeftMotorSpeed(speed);
        setRightMotorSpeed(speed);
        forwards = true;
    }

    // Tight poll ultrasonic distance until target distance is exceeded
    while(true) {
        if(forwards) {
            if(curr_distance < target_distance) {
                break;
            }
        } else {
            if(curr_distance > target_distance) {
                break;
            }
        }
        curr_distance = getDistance();
    }
    driveStop();
}

void turnToAngle(float turn_speed, float target_angle, bool arc_turn) {
    float curr_angle = getTurnAngle();
    TurnDirection direction = (target_angle - curr_angle) > 0 ? CLOCKWISE : COUNTERCLOCKWISE;
    // Flip direction if angle difference is greater than 180
    if (abs(target_angle - curr_angle) > 180) {
        direction = (direction == CLOCKWISE) ? COUNTERCLOCKWISE : CLOCKWISE;
    }

    float arc_ratio = DRIVE_ARC_TURN_RATIO;
    // If arcing backwards, flip ratios
    if(arc_turn && turn_speed < 0) {
        arc_ratio = 1 / arc_ratio;
    }

    if(direction == CLOCKWISE) {
        // Turn right
        if(arc_turn) {
            setLeftMotorSpeed(arc_ratio * turn_speed);
            setRightMotorSpeed((1 / arc_ratio) * turn_speed);
        } else {
            setLeftMotorSpeed(turn_speed);
            setRightMotorSpeed(-turn_speed);
        }
    } else {
        // Turn left
        if(arc_turn) {
            setLeftMotorSpeed((1 / arc_ratio) * turn_speed);
            setRightMotorSpeed(arc_ratio * turn_speed);
        } else {
            setLeftMotorSpeed(-turn_speed);
            setRightMotorSpeed(turn_speed);
        }
    }

    // Tight poll imu yaw angle until target angle is exceeded
    while(true) {
        if(direction == CLOCKWISE) {
            if(curr_angle > target_angle) {
                break;
            }
        } else {
            if(curr_angle < target_angle) {
                break;
            }
        }
        curr_angle = getTurnAngle();
    }
    driveStop();
}

void drivePid(float error, float kp, float kd, float ki, float motor_speed)
{
    // Used for integral term in PID
    static float error_accumulated = 0;
    // Used for derivative term in PID
    static float last_error = 0;
    // Variable to store the time taken to run PID control once (to calculate derivative and integral terms)
    static int last_pid_millis = 0;

    float dt = (millis() - last_pid_millis) / 1000.0; // seconds

    // Serial.println(millis() - last_pid_millis);                                                   
    // Make sure dt does not get too long
    dt = constrain(dt, -0.1, 0.1);

    // K_P
    float p_speed = kp * error;

    // K_I
    error_accumulated += error * dt;
    float i_speed = ki * error_accumulated;

    // K_D
    float error_change = (error - last_error) / dt;
    float d_speed = kd * error_change;

    float speed_difference = p_speed + i_speed + d_speed;

    // Ensure that the speed difference does not get too large
    speed_difference = constrain(speed_difference, -SPEED_DIFFERENCE_CONSTRAINT, SPEED_DIFFERENCE_CONSTRAINT);

    setLeftMotorSpeed(motor_speed - speed_difference);
    setRightMotorSpeed(motor_speed + speed_difference);

    // Store data for next PID loop
    last_error = error;
    last_pid_millis = millis();
}