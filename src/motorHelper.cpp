#include <Arduino.h>
#include "motorHelper.h"

void motorRun(int speed, MotorMode mode, int pin1, int pin2) {
    // Swap the mode and speed pin based on the direction of the motor.
    int mode_pin = speed > 0 ? pin1 : pin2;
    int speed_pin = speed > 0 ? pin2 : pin1;

    int constrained_speed = constrain(abs(speed), ANALOG_LOW, ANALOG_HIGH);

    switch (mode) {
        case MOTOR_BREAK:
            analogWrite(mode_pin, ANALOG_HIGH);
            analogWrite(speed_pin, ANALOG_HIGH - constrained_speed);
            break;
        case MOTOR_COAST:
            analogWrite(mode_pin, ANALOG_LOW);
            analogWrite(speed_pin, constrained_speed);
    }
}