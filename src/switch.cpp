#include "Arduino.h"
#include "switch.h"

static SwitchStatus button_debounce;
static SwitchStatus switch_debounce;

void setupSwitches() {
    pinMode(USER_BTN, INPUT);
    pinMode(LIMIT_SWITCH, INPUT_PULLUP);
}

/**
 * Function: _checkPressed
 * 
 * Detects if a switch/button has been pressed. 
 * Updates vars with the relevant status of the switch/button.
 * 
 * @arg pin: Can be any digital pin. The ones used in this robot:
 *      - USER_BTN is the button on the STM
 *      - LIMIT_SWITCH is the limit switch in the conveyor.
 * @arg &vars: The status variables associated with the given switch/button.
 * 
 * @returns: The current state of the switch/button
*/
static bool _checkPressed(int pin, SwitchStatus &vars) {
    bool current_state = digitalRead(pin);

    // Trigger on falling edge
    if (vars.prev_state == RELEASED && current_state == PRESSED && (millis() - vars.prev_time > DEBOUNCE_TIME_MS))
    {
        vars.prev_time = millis();
        vars.current_state = PRESSED;
    } else {
        vars.current_state = RELEASED;
    }
    vars.prev_state = current_state;
    
    // PRESSED is false
    return vars.current_state == PRESSED;
}

bool checkButtonPressed() {
    return _checkPressed(PUSH_BUTTON, button_debounce);
};

bool checkSwitchPressed() {
    return _checkPressed(LIMIT_SWITCH, switch_debounce);
};