# mte380_robot

The project for the MTE 380 course involves a line-following robot capable of searching for a Lego figure along the line and moving it to a safe location.

## Running the code

[Platformio](https://platformio.org/) for VsCode is used to build the code. External dependencies are listed in `platformio.ini`. The target board is the [STM32F401RE](https://www.st.com/en/microcontrollers-microprocessors/stm32f401re.html).

## Program Structure

Modules:
- Drive motors
- Intake mechanism
- Color sensor (Adafruit_TCS34725)
- Ultrasonic sensor
- Control loop
